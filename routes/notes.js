var express = require("express");
var router = express.Router();
const passport = require("passport");
const Note = require("../models/noteModel");

router.use(passport.authenticate("jwt", { session: false }));

// Получаем весь список заметок текущего пользователя
// данные о пользователе получаем из токена,
// который авторизует пользователя при каждом запросе
router.get("/", function(req, res) {
  Note.find({})
    .populate("user")
    .exec(function(err, notes) {
      if (err) throw err;

      const userNotes = notes.filter(note => {
        return String(note.user._id) === String(req.user._id);
      });

      res.status(200).json(userNotes);
    });
});

// добавляем новую заметку
router.post("/add", function(req, res) {
  const note = {
    ...req.body,
    user: req.user._id
  };

  Note.create(note, function(err, note) {
    if (err) throw err;

    res.status(200).json(note);
  });
});

// удаляем заметку по id
router.delete("/delete", function(req, res) {
  Note.findByIdAndDelete(req.body.id, function(err, result) {
    if (err) throw err;
    res.status(200).send("OK");
  });
});

// изменение содержимого заметки или названия
router.patch("/update", function(req, res) {
  Note.findByIdAndUpdate(req.body.id, { content: req.body.content }, function(
    err,
    doc
  ) {
    if (err) throw err;
    res.status(200).send("UPDATED");
  });
});

module.exports = router;
