var express = require("express");
var router = express.Router();
const passport = require("passport");
const Text = require("../models/textModel");

router.use(passport.authenticate("jwt", { session: false }));

// get all texts of current user
router.get("/", function(req, res, next) {
  Text.find({ user: req.user._id }, function(err, texts) {
    if (err) throw err;

    res.status(200).json(texts);
  });
});

// create new text by current user
router.post("/create", function(req, res) {
  const text = {
    ...req.body,
    user: req.user._id
  };

  Text.create(text, function(error, text) {
    if (error) throw error;

    res.status(200).json({
      success: true,
      msg: `Текст ${text.textName} успешно создан`,
      text
    });
  });
});

// update text by id
router.patch("/save/:id", function(req, res) {
  Text.findByIdAndUpdate(req.params.id, req.body, function(err, result) {
    if (err) {
      res.status(500).json({
        type: "Error",
        msg: "Internal error"
      });
    } else {
      res.status(201).json({
        msg: "Изменения успешно сохранены"
      });
    }
  });
});

router.use(function(err, req, res, next) {
  console.info(err);

  if (req.app.get("env") !== "development") {
    delete err.stack;
  }

  res.status(err.statusCode || 500).json(err);
});

module.exports = router;
