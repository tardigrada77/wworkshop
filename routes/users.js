var express = require("express");
const jwt = require("jsonwebtoken");
const User = require("../models/userModel");

var router = express.Router();

router.post("/signup", function(req, res) {
  if (!req.body.username || !req.body.password) {
    res.status(500).json({
      success: false,
      msg: "Invalid username or password"
    });
  } else {
    const newUser = new User(req.body);

    newUser.save(function(error) {
      if (error)
        return res
          .status(500)
          .json({ success: false, msg: "username already exist" });

      res.status(200).json({
        success: true,
        msg: "Successful created new user"
      });
    });
  }
});

router.post("/signin", function(req, res) {
  const userToken = req.get("Authorization");
  if (userToken) {
    jwt.verify(userToken.slice(4), "secret-word", function(err, decoded) {
      if (err) {
        return verifyByLoginAndEmail(req, res);
      }

      return res.status(200).json({
        success: true,
        username: decoded.username
      });
    });
  } else {
    verifyByLoginAndEmail(req, res);
  }
});

function verifyByLoginAndEmail(req, res) {
  User.findOne({ username: req.body.username }, function(err, user) {
    if (err) throw err;

    if (!user) {
      return res.status(401).send({
        success: false,
        msg: "Authentication failed. User not found"
      });
    }

    user.comparePassword(req.body.password, function(err, isMatch) {
      if (isMatch && !err) {
        const token = jwt.sign(user.toObject(), "secret-word", {
          expiresIn: "1d"
        });

        return res.status(200).json({
          success: true,
          token: `JWT ${token}`,
          username: req.body.username
        });
      } else {
        res.status(401).send({
          success: false,
          msg: "Authentication failed. Wrong password."
        });
      }
    });
  });
}

module.exports = router;
