var express = require("express");
var router = express.Router();
const passport = require("passport");
const Resource = require("../models/ResourceModel");

router.use(passport.authenticate("jwt", { session: false }));

// Ресурсы по id текста
// body = {textID: 4534tgbdfg54}
router.get("/:textID", function(req, res) {
  Resource.find({})
    .populate("Text")
    .exec(function(err, resources) {
      if (err) throw err;

      const filteredResources = resources.filter(resource => {
        return String(resource.text) === req.params.textID;
      });
      console.log(filteredResources);
      res.status(200).json(filteredResources);
    });
});

// Создание нового ресурса
// body = {новый ресурс со всеми полями}
router.post("/create", function(req, res) {
  Resource.create(req.body, function(err, resource) {
    if (err) {
      if (err.code == 11000) {
        res.status(500).send({
          msg: "Ресурс с таким именем уже существует"
        });
      }
    }
    res.status(200).send(resource);
  });
});

// Создание нового ресурса
// body = {id: t5j3kgf854sfdg54}
router.delete("/delete", function(req, res) {
  Resource.findByIdAndDelete(req.body.id, function(err, resource) {
    if (err) throw err;

    res.status(200).json({
      msg: "Успешно удалено",
      resource
    });
  });
});

// Создание нового ресурса
// body = {
//    id: t5j3kgf854sfdg54
//    update: {
//       поля для обновления
//    }
// }
router.patch("/update", function(req, res) {
  Resource.findByIdAndUpdate(req.body.id, { ...req.body.update }, function(
    err,
    resource
  ) {
    if (err) throw err;
    console.log(req.body);
    res.status(200).json({
      msg: "Успешно изменено",
      resource
    });
  });
});

module.exports = router;
