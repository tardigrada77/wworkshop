module.exports = {
    devServer: {
      proxy: {
        "api/": {
          target: "http://localhost:3000/",
          secure: false,
          changeOrigin: true
        }
      }
    },
    outputDir: '../front/',
    assetsDir: '',
    indexPath: 'index.html'
}