import { shallowMount } from "@vue/test-utils";
import Button from "@/components/common/Button.vue";

describe("Button.vue", () => {
  it("renders props.text when passed", () => {
    const text = "new message";
    const wrapper = shallowMount(Button, {
      propsData: { text }
    });
    expect(wrapper.text()).toMatch(text);
  });
});
