import Vue from "../main";

import axios from "axios";

export default {
  state: {
    userNotes: null,
    noteSearchQuery: "",
    baseURL: "/api/notes",
    token: `JWT ${localStorage["token"]}`
  },
  mutations: {
    DELETE_NOTE_BY_ID(state, payload) {
      state.userNotes = state.userNotes.filter(note => {
        return note._id !== payload;
      });
      Vue.$Progress.finish();
    },
    ADD_NEW_NOTE(state, payload) {
      state.userNotes.push(payload);
      Vue.$Progress.finish();
    },
    EDIT_ONE_NOTE(state, payload) {
      const index = state.userNotes.findIndex(item => item._id == payload.id);
      state.userNotes[index].content = payload.text;
      Vue.$Progress.finish();
    },
    SET_NOTE_QUERY_STRING(state, payload) {
      state.noteSearchQuery = payload;
    },
    SET_NOTES(state, payload) {
      state.userNotes = payload;
      Vue.$Progress.finish();
    }
  },
  actions: {
    DELETE_NOTE({ commit, state }, payload) {
      Vue.$Progress.start();

      const url = `${state.baseURL}/delete`;

      const userToken = `JWT ${localStorage["token"]}`;
      const config = {
        headers: { Authorization: userToken },
        data: {
          id: payload
        }
      };

      axios
        .delete(url, config)
        .then(res => {
          if (res.status == 200) {
            commit("DELETE_NOTE_BY_ID", payload);
          }
        })
        .catch(err => {
          Vue.$Progress.fail();
          console.log(err);
        });
    },
    ADD_NOTE({ commit, state }, payload) {
      Vue.$Progress.start();

      const url = `${state.baseURL}/add`;
      const data = {
        title: payload.title,
        content: payload.content
      };

      const userToken = state.token;

      const config = {
        headers: { Authorization: userToken }
      };
      axios
        .post(url, data, config)
        .then(res => {
          console.log(res.data);
          const newNote = res.data;
          commit("ADD_NEW_NOTE", newNote);
        })
        .catch(err => {
          Vue.$Progress.fail();
          console.log(err);
        });
    },
    EDIT_NOTE({ commit, state }, payload) {
      Vue.$Progress.start();

      const url = `${state.baseURL}/update`;
      const data = {
        id: payload.id,
        content: payload.text
      };

      const userToken = state.token;

      const config = {
        headers: { Authorization: userToken }
      };

      axios
        .patch(url, data, config)
        .then(res => {
          console.log(res);
        })
        .catch(err => {
          console.log(err);
          Vue.$Progress.fail();
        });

      commit("EDIT_ONE_NOTE", payload);
    },
    GET_NOTES({ commit, state }) {
      Vue.$Progress.start();

      const url = `${state.baseURL}/`;
      const userToken = state.token;

      const config = {
        headers: { Authorization: userToken }
      };

      axios
        .get(url, config)
        .then(res => {
          commit("SET_NOTES", res.data);
        })
        .catch(err => {
          console.log(err);
          Vue.$Progress.fail();
        });
    }
  },
  getters: {
    notes: state => state.userNotes,
    filteredNotes: state => {
      if (state.userNotes) {
        return state.userNotes.filter(item => {
          return (
            item.title
              .toLowerCase()
              .includes(state.noteSearchQuery.toLowerCase()) ||
            item.content
              .toLowerCase()
              .includes(state.noteSearchQuery.toLowerCase())
          );
        });
      }
    }
  }
};
