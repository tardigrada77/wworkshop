import faker from "faker";
// import axios from 'axios';

export const mockMedia = () => {
  let images = [];
  for (let i = 0; i < 7; i++) {
    images.push(`${faker.random.image()}/3`);
  }
  return images;
};

export const mockResources = () => {
  return [
    {
      _id: faker.random.uuid(),
      type: {
        name: "Событие",
        icon: "search"
      },
      title: "Битва за луну"
    },
    {
      _id: faker.random.uuid(),
      type: {
        name: "Персонаж",
        icon: "accessibility"
      },
      title: "Джон сноу"
    },
    {
      _id: faker.random.uuid(),
      type: {
        name: "История",
        icon: "history"
      },
      title: "Вестерос"
    },
    {
      _id: faker.random.uuid(),
      type: {
        name: "Событие",
        icon: "search"
      },
      title: "Красная свадьба"
    },
    {
      _id: faker.random.uuid(),
      type: {
        name: "Место",
        icon: "place"
      },
      title: "Стена"
    }
  ];
};

const texts = [
  {
    _id: 1,
    title: "Chapter #1 longtitle of chapter",
    resources: mockResources(),
    pages: 1
  },
  {
    _id: 2,
    title: "Chapter #2 longtitle of chapter",
    resources: mockResources(),
    pages: 1
  },
  {
    _id: 3,
    title: "Chapter #3 longtitle of chapter",
    resources: mockResources(),
    pages: 1
  }
];

const mockNotes = [
  {
    id: faker.random.uuid(),
    title: faker.random.word(),
    text: faker.lorem.paragraph()
  },
  {
    id: faker.random.uuid(),
    title: faker.random.word(),
    text: faker.lorem.paragraph()
  },
  {
    id: faker.random.uuid(),
    title: faker.random.word(),
    text: faker.lorem.paragraph()
  },
  {
    id: faker.random.uuid(),
    title: faker.random.word(),
    text: faker.lorem.paragraph()
  }
];

export const mockTexts = () => {
  return texts;
};

export const getCurrentText = _id => {
  return texts.filter(text => {
    return text._id == _id;
  })[0];
};

export const getMockNotes = () => {
  return mockNotes;
};

export const addMockNote = () => {
  return {
    id: faker.random.uuid(),
    title: faker.random.word(),
    text: faker.lorem.paragraph().slice(0, 200)
  };
};
