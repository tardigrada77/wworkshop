import { mockResources, getCurrentText } from './mock';
import axios from 'axios';
import vue from '../main';
import store from '../store';

export default {
  state: {
    // currentText: {
    //     title: 'CURRENT',
    //     resources: mockResources(),
    // },
    currentText: null,
    resourceSidebarState: 0,
    resourceSearchQuery: '',
    fullscreenMediaSlider: {
      isOpen: false
    },
    requestData: {
      token: `JWT ${localStorage.getItem('token')}`,
      baseTextUrl: 'api/texts',
      baseResourcesUrl: 'api/resources'
    }
  },
  mutations: {
    // slider
    CLOSE_MEDIA_SLIDER(state) {
      state.fullscreenMediaSlider.isOpen = false;
    },
    OPEN_MEDIA_SLIDER(state) {
      state.fullscreenMediaSlider.isOpen = true;
    },

    // resource list in editor
    SELECT_RESOURCE(state, payload) {
      store.commit('SET_CURRENT_RESOURCE', payload);
      state.resourceSidebarState = 1;
    },
    OPEN_RESOURCES_LIST(state) {
      state.resourceSidebarState = 0;
    },

    // resources page
    SET_CURRENT_TEXT(state, payload) {
      if (payload === null) {
        state.currentText = null;
      } else {
        const texts = store.getters.currentUserTexts;

        const currentText = texts.filter(text => {
          return text._id == payload.id;
        });

        // currentText.resources = mockResources();
        currentText[0].resources = payload.resources;

        state.currentText = currentText[0];

        vue.$Progress.finish();
      }
    },

    ADD_NEW_RESOURCE_AFTER_CREATING(state, payload) {
      // state.currentText.resources.push(payload);
      // console.log(state.currentText.resources)

      store.dispatch('CHANGE_CURRENT_TEXT', payload.text);
    },

    // поиск в ресурсах
    SET_RESOURCE_QUERY_STRING(state, payload) {
      state.resourceSearchQuery = payload;
    },
    SET_RESOURCES_FOR_CURRENT_TEXT(state, payload) {
      state.currentText.resources = payload;
    },

    //editor content
    SET_TEXT_CONTENT(state, payload) {
      state.currentText.content = payload;
    }
  },
  actions: {
    CHANGE_CURRENT_TEXT({ commit, state }, payload) {
      vue.$Progress.start();
      const baseUrl = state.requestData.baseResourcesUrl;
      const userToken = state.requestData.token;
      const url = `${baseUrl}/${payload}`;

      const config = {
        headers: { Authorization: userToken }
      };

      axios
        .get(url, config)
        .then(res => {
          const data = {
            id: payload,
            resources: res.data
          };
          commit('SET_CURRENT_TEXT', data);
        })
        .catch(err => {
          console.log(err);
          vue.$Progress.fail();
        });
    },
    CREATE_NEW_TEXT({ commit, state }, payload) {
      const baseUrl = state.requestData.baseTextUrl;
      const url = `${baseUrl}/create`;
      const userToken = state.requestData.token;

      const data = {
        textName: payload,
        content: ''
      };

      const config = {
        headers: { Authorization: userToken }
      };

      axios
        .post(url, data, config)
        .then(res => {
          store.dispatch('GET_CURRENT_USER_TEXTS');
          store.dispatch('CHANGE_CURRENT_TEXT', res.data.text._id);
        })
        .catch(err => {
          console.log(err);
          vue.$Progress.fail();
        });
    },
    SAVE_TEXT({ commit, state }, payload) {
      // console.log('from store  on save -->', payload)
      const id = state.currentText._id;
      const baseUrl = state.requestData.baseTextUrl;
      const url = `${baseUrl}/save/${id}`;
      const userToken = state.requestData.token;

      const data = {
        content: payload
      };
      const config = {
        headers: { Authorization: userToken }
      };

      axios
        .patch(url, data, config)
        .then(res => {
          commit('SET_TEXT_CONTENT', payload);
          vue.$toasted.show(res.data.msg, {
            type: 'success',
            icon: 'check_circle',
            duration: 3000,
            position: 'bottom-center'
          });
        })
        .catch(err => {
          console.log(err);
        });
    }
  },
  getters: {
    currentText: state => state.currentText,
    currentResources: state => {
      if (state.currentText) return state.currentText.resources;
    },
    mediaSliderIsOpen: state => state.fullscreenMediaSlider.isOpen,
    currentSidebarState: state => state.resourceSidebarState,
    filteredResources: state => {
      if (state.currentText === null) return [];
      return state.currentText.resources.filter(res => {
        return res.name
          .toLowerCase()
          .includes(state.resourceSearchQuery.toLowerCase());
      });
    }
  }
};
