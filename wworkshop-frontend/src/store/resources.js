import { mockMedia } from "./mock";
import axios from "axios";

import Vue from "../main";
import store from "../store";

export default {
  state: {
    currentResource: {
      title: "",
      type: {
        name: "Событие",
        icon: "search"
      },
      media: mockMedia()
    },
    baseURL: "/api/resources",
    token: `JWT ${localStorage["token"]}`,
    types: [
      {
        name: "Событие",
        icon: "search"
      },
      {
        name: "История",
        icon: "history"
      },
      {
        name: "Место",
        icon: "place"
      },
      {
        name: "Персонаж",
        icon: "accessibility"
      }
    ],
    resourcesPageState: 0,
    isCreatingResource: false
  },
  mutations: {
    RESOURCE_PAGE_STATE(state, payload) {
      // payload --> 0, 1
      state.resourcesPageState = payload;
    },
    SET_CREATING_STATUS(state, payload) {
      state.isCreatingResource = payload;
    },

    SET_CURRENT_RESOURCE(state, payload) {
      state.currentResource = store.getters.currentResources.filter(res => {
        return res._id == payload;
      })[0];
    }
  },
  actions: {
    CREATE_NEW_RESOURCE({ commit, state }, payload) {
      Vue.$Progress.start();

      const url = `${state.baseURL}/create`;
      const data = payload;
      const userToken = state.token;
      const config = {
        headers: { Authorization: userToken }
      };

      axios
        .post(url, data, config)
        .then(res => {
          store.commit("ADD_NEW_RESOURCE_AFTER_CREATING", res.data);
          const fake = store.getters.filteredResources;
          Vue.$Progress.finish();
        })
        .catch(err => {
          console.log(err);
          Vue.$Progress.fail();
        });
    }
  },
  getters: {
    currentResource: state => state.currentResource,
    currentResourcesPageState: state => state.resourcesPageState,
    isCreatingNewResource: state => state.isCreatingResource,
    getTypes: state => state.types
  }
};
