import axios from 'axios';
import router from '../router';
import vue from '../main';

import Vue from 'vue';

export default {
  state: {
    user: {
      isAuthenticated: false,
      username: '',
      texts: []
    }
  },
  mutations: {
    SET_USER(state, payload) {
      state.user.isAuthenticated = true;
      state.user.username = payload.username;

      router.push('editor');
    },
    UNSET_USER(state) {
      state.user = {
        isAuthenticated: false,
        username: '',
        texts: []
      };

      localStorage.setItem('token', null);
      router.push('login');
    },
    SET_TEXTS(state, payload) {
      console.log('[SET_TEXTS]', payload);
      state.user.texts = payload;
    }
  },
  actions: {
    SIGNIN({ commit, dispatch }, payload) {
      let token = '';
      if (
        localStorage.getItem('token') &&
        localStorage.getItem('token').length > 0
      ) {
        token = `JWT ${localStorage.getItem('token')}`;
      }

      let data = {};
      if (payload) {
        data.username = payload.login;
        data.password = payload.password;
      }

      let config = {};
      if (token) {
        config.headers = {
          Authorization: token
        };
      }

      const url = '/api/users/signin';

      axios
        .post(url, data, config)
        .then(response => {
          if (response.data.token) {
            const token = response.data.token.slice(4);
            localStorage.setItem('token', token);
          }

          dispatch('GET_CURRENT_USER_TEXTS');
          commit('SET_USER', response.data);
        })
        .catch(error => {
          vue.$toasted.show(error.response.data.msg, {
            type: 'error',
            icon: 'warning',
            duration: 3000
          });
        });
    },
    SIGNUP({ commit }, payload) {
      const url = '/api/users/signup';
      axios
        .post(url, {
          username: payload.login,
          password: payload.password
        })
        .then(() => {
          router.push('login');
        })
        .catch(error => {
          vue.$toasted.show(error.response.data.msg, {
            type: 'error',
            icon: 'warning',
            duration: 3000
          });
        });
    },
    GET_CURRENT_USER_TEXTS({ commit }) {
      const url = '/api/texts/';
      const token = `JWT ${localStorage.getItem('token')}`;
      const config = {
        headers: { Authorization: token }
      };
      axios
        .get(url, config)
        .then(res => {
          commit('SET_TEXTS', res.data);
        })
        .catch(err => {
          console.log(err);
        });
    }
  },
  getters: {
    isUserAuthenticated: state => state.user.isAuthenticated,
    currentUser: state => state.user,
    currentUserTexts: state => state.user.texts
  }
};
