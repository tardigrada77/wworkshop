import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

// Vue.config.productionTip = false

import VueNotifications from "vue-notifications";
import VueToasted from "vue-toasted";

function toast({ message, type, timeout }) {
  if (type === VueNotifications.types.warn) type = "show";
  return Vue.toasted[type](message, {
    duration: timeout,
    position: "bottom-center"
  });
}

Vue.use(VueToasted);

const options = {
  success: toast,
  error: toast,
  info: toast,
  warn: toast
};

Vue.use(VueNotifications, options);

import VueProgressBar from "vue-progressbar";

Vue.use(VueProgressBar, {
  color: "#ffab00",
  failedColor: "red",
  // в документации написано height, а в исходниках thickness
  thickness: "3px"
});

//директива для отложенного сохранения
Vue.directive("ondelay", {
  bind(el, options) {
    let timer;
    let timeout = 0;

    for (let name in options.modifiers) {
      if (!isNaN(+name)) {
        timeout = parseInt(name);
      }
    }

    let callback = e => {
      if (timer !== undefined) {
        clearInterval(timer);
      }

      if (options.modifiers.prevent) {
        e.preventDefault();
      }

      timer = setTimeout(() => {
        options.value.call(this, e);
      }, timeout);
    };

    el.addEventListener(options.arg, callback);
  }
});

export default new Vue({
  router,
  store,
  created() {
    if (localStorage["token"].length > 0) {
      store.dispatch("SIGNIN");
    } else {
      router.push("login");
    }
  },
  render: h => h(App)
}).$mount("#app");
