import Vue from "vue";
import Router from "vue-router";
import Notes from "./views/Notes.vue";
import Editor from "./views/Editor.vue";
import Profile from "./views/Profile.vue";
import Resources from "./views/Resources.vue";
import Login from "./views/Login.vue";
import Signup from "./views/Signup.vue";

import store from "./store";

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: "/",
      component: Editor,
      beforeEnter: authGuard
    },
    {
      path: "/editor",
      name: "editor",
      component: Editor,
      beforeEnter: authGuard
    },
    {
      path: "/notes",
      name: "notes",
      component: Notes,
      beforeEnter: authGuard
    },
    {
      path: "/profile",
      name: "profile",
      component: Profile,
      beforeEnter: authGuard
    },
    {
      path: "/resources",
      name: "resources",
      component: Resources,
      beforeEnter: authGuard
    },
    {
      path: "/login",
      name: "login",
      component: Login
    },
    {
      path: "/signup",
      name: "signup",
      component: Signup
    }
  ],
  mode: "history"
});

function authGuard(to, from, next) {
  if (store.getters.isUserAuthenticated) {
    next();
  } else {
    next("login");
  }
}

export default router;
