import Vue from "vue";
import Vuex from "vuex";

import userModule from "./store/user";
import generalModule from "./store/general";
import notesModule from "./store/notes";
import resourcesModule from "./store/resources";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    userModule,
    generalModule,
    notesModule,
    resourcesModule
  }
});
