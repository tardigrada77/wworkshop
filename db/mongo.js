const mongoose = require('mongoose');
const config = require('./config.json');

mongoose.Promise = global.Promise;

// const connectionURL = `mongodb://${config.db.user}@${config.db.host}:${config.db.port}/${config.db.name}`;
const mlabConnectionUrl = `mongodb://${config.db.user}:${config.db.password}@ds261096.mlab.com:61096/wworkshop`;

mongoose.connect(mlabConnectionUrl, {useNewUrlParser: true})
        .catch((e) => console.error(e))
    
const db = mongoose.connection;

db.on('connected', () => console.log(`Соединение успешно --> ${mlabConnectionUrl}`));

db.on('error', (error) => console.error(error));

db.on('disconnected', () => console.log('Соединение разорвано'));

process.on('SIGINT', () => {
    db.close(() => {
        console.log('Соединение было разорвано');
        process.exit(0);
    })
})