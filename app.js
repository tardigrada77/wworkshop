var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

const db = require("./db/mongo");

const passport = require("passport");
require("./db/passport")(passport);

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
const textsRouter = require("./routes/texts");
const notesRouter = require("./routes/notes");
const resourcesRouter = require("./routes/resources");

var app = express();

app.use(passport.initialize());

// view engine setup
app.set("views", path.join(__dirname, "front"));
app.engine("html", require("ejs").renderFile);
app.set("view engine", "html");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "front")));

app.use("/api/users", usersRouter);
app.use("/api/texts", textsRouter);
app.use("/api/resources", resourcesRouter);
app.use("/api/notes", notesRouter);
app.use("/*", indexRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
