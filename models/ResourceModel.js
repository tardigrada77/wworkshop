const mongoose = require("mongoose");

const ResourceSchema = new mongoose.Schema({
  name: {
    type: String,
    unique: true,
    required: true
  },
  typeName: {
    type: String,
    required: true
  },
  typeIcon: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  shortDescription: {
    type: String,
    required: true
  },
  media: {
    // [link1, link2]
    type: Array,
    required: false
  },
  customFields: {
    // {title: '', content: ''}
    type: Array,
    required: false
  },
  text: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Text",
    required: true
  }
});

module.exports = mongoose.model("Resource", ResourceSchema);
