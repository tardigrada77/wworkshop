const mongoose = require("mongoose");

const TextSchema = new mongoose.Schema({
  textName: {
    type: String,
    unique: true,
    required: true
  },
  content: {
    type: String,
    required: false
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true
  }
});

module.exports = mongoose.model("Text", TextSchema);
